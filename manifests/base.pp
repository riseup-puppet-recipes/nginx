class nginx::base {

  package { 'nginx':
    ensure => installed,
  }
  
  service { 'nginx':
    ensure => running,
    enable => true,
    hasstatus => false, 
    require => Package[nginx],
  }
  
  file { nginx_config:
    path => '/etc/nginx/nginx.conf',
    source => [ "puppet:///modules/site_nginx/${fqdn}/nginx.conf",
                "puppet:///modules/site_nginx/nginx.conf",
                "puppet:///modules/nginx/nginx.conf" ],
    owner => root,
    group => 0,
    mode => 0644,
    require => Package[nginx],
    notify => Service[nginx],
  }

  # Notify the reload-nginx exec when nginx needs to be reloaded, this is
  # useful when sites are added and removed, as it is not necessary to do
  # a full restart.
  exec { "reload-nginx":
    command => "/etc/init.d/nginx reload",
    refreshonly => true,
    before => [ Service["nginx"], Exec["force-reload-nginx"] ]
  }

  # This is only needed for certain situations
  exec { "force-reload-nginx":
    command => "/etc/init.d/nginx force-reload",
    refreshonly => true,
    before => Service["nginx"],
  }
  augeas {
    'logrotate_nginx':
      context => '/files/etc/logrotate.d/nginx/rule',
      changes => [ 'set rotate 14','set schedule daily','set minsize 50M' ]
  }

  if $use_munin {
    include nginx::munin
  }
  
}
