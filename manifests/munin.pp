class nginx::munin inherits munin::plugins::base {

  nginx::vhost { 'munin': }

  package { "libwww-perl": ensure => installed; }
  
  munin::plugin::deploy {
    'nginx_request':
      source => 'nginx/munin/nginx_request',
      config => 'env.url http://localhost/nginx_status';

    'nginx_status':
      source => 'nginx/munin/nginx_status',
      config => 'env.url http://localhost/nginx_status';

    'nginx_memory':
      source => 'nginx/munin/nginx_memory',
      config => 'env.url http://localhost/nginx_status';
  }
}
