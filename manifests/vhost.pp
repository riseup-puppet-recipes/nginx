# simple vhost support for nginx.
# it just copies the vhost file to /etc/nginx/sites-enabled
# most of the logic was copied from the apache shared module.

define nginx::vhost(
    $ensure = present,
    $content = 'absent',
    $vhost_source = 'absent'
){

  $nginx_sites = "/etc/nginx/sites"
  $nginx_available_file = "${nginx_sites}-available/${name}.conf"
  $nginx_enabled_file = "${nginx_sites}-enabled/${name}.conf"
  $nginx_enabled_file_ensure = $ensure ? { 'absent' => 'absent', default => "${nginx_sites}-available/${name}.conf" }
  $n2site_exec = $ensure ? { 'absent' => "/bin/rm ${nginx_sites}-enabled/${name}.conf", 'present' => "/bin/ln -s ${nginx_sites}-available/${name}.conf ${nginx_sites}-enabled/${name}.conf" }

  file { $nginx_available_file:
    ensure => $ensure,
    notify => Service[nginx],
    owner => root, group => 0, mode => 0644;
  }

  case $content {
    'absent': {
      $real_vhost_source = $vhost_source ? {
        'absent'  => [
                      "puppet://$server/modules/site_nginx/vhosts.d/$fqdn/$name.conf",
                      "puppet://$server/modules/site_nginx/vhosts.d/$operatingsystem.$lsbdistcodename/$name.conf",
                      "puppet://$server/modules/site_nginx/vhosts.d/$operatingsystem/$name.conf",
                      "puppet://$server/modules/site_nginx/vhosts.d/$name.conf",
                      "puppet://$server/modules/nginx/vhosts.d/$name.conf",
                      "puppet://$server/modules/nginx/vhosts.d/$operatingsystem.$lsbdistcodename/$name.conf",
                      "puppet://$server/modules/nginx/vhosts.d/$operatingsystem/$name.conf",
                      "puppet://$server/modules/nginx/vhosts.d/$name.conf"
                      ],
        default => "puppet:///$vhost_source",
      }
      File[$nginx_available_file]{
        source => $real_vhost_source,
      }
    }
    default: {
      File[$nginx_available_file]{
        content => $content,
      }
    }
  }

  file {
    $nginx_enabled_file:
      ensure => $nginx_enabled_file_ensure,
      mode => 0644, owner => root, group => root,
      require => Exec["n2site-${name}.conf"],
      notify => Exec["reload-nginx"];
  }

  exec { $n2site_exec:
    refreshonly => true,
    notify => Exec["reload-nginx"],
    alias => "n2site-${name}.conf";
  }
}
