class nginx::debian inherits nginx::base {

  Service[nginx]{ hasstatus => true, hasrestart => true }
}
